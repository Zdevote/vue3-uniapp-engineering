/*
 * @Author: CtrlC
 * @Date: 2022-07-26 17:32:46
 * @LastEditors: CtrlC
 * @LastEditTime: 2022-07-26 17:46:03
 * @Description: 订单API
 * @FilePath: /vue3-uniapp-engineering/src/api/order.js
 */
const { http } = uni.$u

/**
 * @description: 提交订单
 */
export const orderSubmit = (
  params,
  config = {
    custom: {
      catch: true
    }
  }
) => http.post('order/submit', params, config)
/**
 * @description: 提交列表
 */
export const orderList = (
  params,
  config = {
    custom: {
      catch: true
    }
  }
) => http.post('order/orderList', params, config)
