/*
 * @Author: CtrlC
 * @Date: 2024-01-17 10:19:12
 * @Description: APP 环境变量
 * @FilePath: /vue3-uniapp-engineering/src/static/app-plus/config.js
 */
let Config = {
  initEnvironment: () => {
    /**
     * @description: environment
     * @return {*}   dev:开发，test：测试，pre：预发，prod：正式
     */
    let environment = 'test'
    const ENV = {}
    switch (environment) {
      case 'local':
        // 开发
        ENV.VITE_BASE_API = '/api'
        break
      case 'dev':
        // 开发
        ENV.VITE_BASE_API = 'api'
        break
      case 'test':
        ENV.VITE_BASE_API = '/api'
        break
      case 'pre':
        ENV.VITE_BASE_API = '/api'
        break
      case 'prod':
        ENV.VITE_BASE_API = '/api'
        break
      default:
        uni.showModal({
          content: '未知环境，请联系管理员！'
        })
        break
    }
    Config = Object.assign(Config, ENV)
  }
}
Config.initEnvironment()

export default Config