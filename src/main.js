/*
 * @Author: CtrlC
 * @Date: 2022-06-30 20:30:44
 * @LastEditors: CtrlC
 * @LastEditTime: 2022-08-05 15:11:09
 * @Description:  app全局配置
 * @FilePath: /vue3-uniapp-engineering/src/main.js
 */
import { createSSRApp } from 'vue'
import uviewPlus from 'uview-plus'
import App from './App.vue'
import envConfig from './common/envConfig'
import request from '@/utils/request'

export default function createApp() {
  const app = createSSRApp(App)
  app.use(uviewPlus).use(envConfig)
  request(app)
  return {
    app
  }
}
