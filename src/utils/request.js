/*
 * @Author: CtrlC
 * @Date: 2022-07-11 14:26:03
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-01-17 10:26:08
 * @Description: 请求类
 * @FilePath: /vue3-uniapp-engineering/src/utils/request.js
 */
import httpGlobalConfig from '../common/httpGlobalConfig'

export default app => {
  const { VITE_BASE_API } = app.$env
  uni.$u.http.setConfig(config => {
    config.baseURL = VITE_BASE_API /* 根域名 */
    config.custom = httpGlobalConfig
    return config
  })
  uni.$u.http.interceptors.request.use(
    config => config,
    config => Promise.reject(config)
  )
  uni.$u.http.interceptors.response.use(
    response => {
      const { data } = response
      // 自定义参数
      const custom = response.config?.custom
      if (data.code !== 200) {
        if (custom.toast !== false) {
          uni.$u.toast(data.message)
        }
        if (custom?.catch) {
          return Promise.reject(data)
        }
        return new Promise(() => {})
      }
      return data.data || {}
    },
    response => Promise.reject(response)
  )
}
