/*
 * @Author: CtrlC
 * @Date: 2022-07-26 17:00:43
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-01-17 10:18:31
 * @Description: 环境变量配置
 * @FilePath: /vue3-uniapp-engineering/src/common/envConfig.js
 */

import APP_ENV from '@/static/app-plus/config.js'
// 运行的平台
const RUNTIME_PLATFORM = uni.$u.platform

// eslint-disable-next-line prefer-destructuring
let env = import.meta.env
// #ifdef APP-PLUS
env = APP_ENV
// #endif
// 运行的系统
const OS_SYSTEM = uni.$u.os()
export default {
  install(app) {
    app.$env = {
      // 公共参数
      APP_NAME: '',
      VERSION: '1.0.0',
      RUNTIME_PLATFORM,
      OS_SYSTEM,
      // 环境变量
      ...env
    }
  }
}
