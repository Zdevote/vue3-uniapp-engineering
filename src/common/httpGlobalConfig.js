/*
 * @Author: CtrlC
 * @Date: 2023-10-19 17:13:13
 * @Description: http 全局配置
 * @FilePath: /cz_uniapp_local/src/common/httpGlobalConfig.js
 */

export default {
  custom: {
    auth: true
  },
  timeout: 50000
}
