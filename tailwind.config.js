/*
 * @Author: CtrlC
 * @Date: 2024-07-10 15:28:22
 * @Description: file content
 * @FilePath: /vue3-uniapp-engineering/tailwind.config.js
 */
/** @type {import('tailwindcss').Config} */
// const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./src/**/*.{vue,js,ts,jsx,tsx,html}'],
  important: true,
  mode: 'jit',
  theme: {
    extend: {
      colors: {}
    }
  },
  plugins: [],
  corePlugins: {
    preflight: false
  }
}
