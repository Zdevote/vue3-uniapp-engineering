<!--
 * @Author: CtrlC
 * @Date: 2022-07-26 17:54:30
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-07-10 15:53:35
 * @Description: file content
 * @FilePath: /vue3-uniapp-engineering/README.md
-->
# vue3-uniapp-engineering

#### 介绍
一套完整，规范的基于vue3，vite，eslint，commitlint，uniapp的工程环境
#### 架构目录说明
  * .husky      git提交钩子
  * pages       页面组件
  * api         接口管理
    > order.js为使用事例
  * common      公共类
    + const.js  全局常量
    + envConfig 环境变量
    + uviewConfing uview全局配置
  * components  公共组件
  * static      静态文件
    + app-plus        APP环境变量
  * uni_modules ui组件
  * utils       工具类
  * .cz-config  git提交配置
  * .eslintrc   eslint配置
  * tailwind.config   tailwind配置项
#### 架构配置说明
##### 集成 EditorConfig 配置
  > 处理同意项目多个开发人员维护一致的编码风格
  > 官网：[editorconfig.org](https://editorconfig.org/).
##### 集成 Prettier 配置
  > 官网：[prettier.io](https://prettier.io/).
##### 集成eslint
  > 官网：[eslint](https://github.com/eslint/eslint)
##### 集成husky
  > 官网：[husky](https://github.com/typicode/husky)
  > git hooks工具，可以设置git在各个阶段的触发命令
##### 集成lint-staged
  > 官网：[lint-staged](https://github.com/okonet/lint-staged)
  > 在 git 暂存的文件上运行 linters。
##### 集成tailwind  
  > 官网：[tailwindcss](https://tailwindcss.com/)
  > vscode 插件 Tailwind CSS IntelliSense
#### 安装教程
1. npm i pnpm -g
2. pnpm i (node 18 以上)
3. 启动命令
     ```js
     //h5环境
      pnpm run dev
     //小程序
      pnpm run mp-weixin
     //APP
      自行修改 src/static/app-plus
     ```
    * 可安装vscode插件(npm Intellisense)，一键运行
#### 常见问题
  + git cz 无命令 (全局安装commitizen)
  > pnpm install commitizen -g
#### 使用说明
1.  使用vscode进行开发，安装Prettier插件
2.  git 提交规范，参考Angular提交规范
    > fix(other):修改 commit-msg执行脚本
3.  命令行使用git cz进行提交
4.  可视化工具进行提交也会触发钩子
5.  vue2项目，请自行修改vite.config.js