/*
 * @Author: CtrlC
 * @Date: 2022-07-26 17:00:14
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-07-10 15:27:45
 * @Description: file content
 * @FilePath: /vue3-uniapp-engineering/vite.config.js
 */
import { defineConfig } from 'vite'
import uni from '@dcloudio/vite-plugin-uni'
import eslintPlugin from 'vite-plugin-eslint'
import tailwindcss from 'tailwindcss'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    uni(),
    eslintPlugin({
      cache: false
    })
  ],
  css: {
    postcss: {
      // 内联写法
      plugins: [tailwindcss()]
    }
  }
})
